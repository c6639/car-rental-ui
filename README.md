# Noleggio macchine - car rental

This is a little example of FE based on React v.17 and Redux (through redux-toolkit).  
I developed this example partly to learn MaterialUI v.5, hence several of its components are used. For the edition of date fields, I rely on react-day-picker.  
It's a simulation of a car rental operation.  
The language used is Italian.

## Play with the example.
To make the example work, just clone the repo, and then `npm install & npm start`. Then just write `localhost:3000` in a browser's address bar.

In the first page, the dates and places of pick-up and return must be entered. The dates cannot be in the past, and return date cannot be before pick-up date, while the places should just not be empty. When all the data is correctly entered, the user can click the "Cerca auto", then a (fake) list of cars available ("vetture disponibili") for the given period appears. 

![Choose car](./readme_images/scegliereMacchina.jpg)

Clicking "Noleggiami" for a car triggers the second page, in which the user types the customer data.  

![Customer data](./readme_images/datiCliente.jpg)

In the customer data page, filling at least one field among name, phone number and mail enables the "Cerca" button. Pushing that button let the user choose a customer whose data is similar to that entered, the options appear in a popup (if no existing customer is similar to the entered data, then a little popup describing this situation is shown).
The users come from a fake data set included in the project. To try a search with several matches, you can type "scirea" as surname.  

![Customer selection](./readme_images/sceltaClienteEsistente.jpg)

When an existing customer is chosen, the label beneath the customer data changes from "Cliente nuovo" to "Cliente esistente". Afterwards, the user can change the name, phone number and/or mail address.  
The button "Pulisci" clears all the customer data.

From this page, the user can get back to the previous one by clicking on the "Cambia" button that shows to the right of the car ("Macchina") and period details.

### Customer data consolidation
When enough (name + either phone or mail) customer data is entered, the green "Conferma nuovo noleggio" is enabled. If some data of an existing customer has changed, then a consolidation popup appears.  
The popup shows to the left ("Dati in DB") the current data for the customer, and to the right ("Dati inseriti ora") the data in the just filled form.

Different icons shown at the right of some data items allows to perform consolidation actions.

- To delete a current phone number or email (on the left panel), click the delete ![Delete](./readme_images/eliminare.jpg) button. The data to be deleted appears in grey and with striketrough.
- To cancel a delete indication (on the left panel), click the restore ![Restore](./readme_images/cancellareEliminazione.jpg) button.
- To add a modified phone number or email (on the right panel), or change the name, click the "pass" ![Pass](./readme_images/passare.jpg) button. The data item will be shown in blue in both left and right panels. For a name change, the current name is shown as deleted in the left panel.
- To cancel an addition/modification, click the "unpass" ![Unpass](./readme_images/cancellarePassagio.jpg) button.

An example of the customer consolidation popup follows. In this case, the user changed the name (adding the middle name "Catena") and the phone number (changing the last digit from 9 to 8 from the first number in the list). The popup appears as follows.

![Initial scenario](./readme_images/consolidazioneScenarioIniziale.jpg)

There is no ![Pass](./readme_images/passare.jpg) button corresponding to the mail address, since the user didn't change it.  
If he user decides to add the changed phone number and delete the "original" number, but not to record the change made in the name, then the popup changes to this.

![First change scenario](./readme_images/consolidazioneScenario1.jpg)

If afterwards she clicks the ![Pass](./readme_images/passare.jpg) to also record the change on the name, then the popup looks like this.

![Second change scenario](./readme_images/consolidazioneScenario2.jpg)

If after this, the user decides to keep the 9-ended number and not to insert the 8-ended one (by clicking the ![Restore](./readme_images/cancellareEliminazione.jpg) and ![Unpass](./readme_images/cancellarePassagio.jpg) icons respectively), the popup changes to this.

![Third change scenario](./readme_images/consolidazioneScenario3.jpg)

The "Tornare a dati noleggio" gets back to the edition, while "Registrare noleggio" ends the operation.



# Design and implementation notes

## Organization of the code
The source code is organized in the following folders
- _pages_: the components accessed via navigation, that control the whole screen. There are currently two pages: `RentalInitialParameters` and `RentalCustomerData`.
- _components_: the project-specific components used in pages, including both generic and domain-specific components. Includes the modals used in `RentalCustomerData` for customer selection, customer consolidation and finalization.
- _services_: functions that implement calls to the BE. The current implementation consists of fakes for the eventually available BE endpoints.
- _domain_: functions and hooks that could be used in different pages/component of a given business domain.
- _hooks_: the generic hooks implemented for the project.
- _store_: configuration of the Redux store through redux-toolkit.
- _utils_: some generic functions, includes the custom theme for Material UI.

## State management
All the state that could be used in multiple components pertaining to a specific use case is stored in the Redux store. At the moment, the Redux store includes just one slice, since the example involves only one use case.

The data stored in this slice includes: data obtained from the BE (e.g. as the list of available cars), the state of the input fields, and other decisions made by the user such the chosen car and the changes in the eventual consolidation process.

### Input data
The Redux slice includes one variable that stores the relevant information about each input field, that consists of: value, inputValue, errorText, helperText, isValid, touched.  
We mantain two separate inputValue and value attributes since for dates, the value handled by the actual input component (a JS Date) differs from that I decided to use for the navigation logic (a Luxon DateTime). In other projects, I used the same idea to also handle boolean inputs as "Yes/No" radio groups.

A `useUpdateForm` hook defines functions (e.g. `updateTextField`, `updateTextField` or `clearTextField`) that simplify the handling of such state. Rather than having an internal storage, this hook receives in its constructor a reference to a structure that must include a low-level `updateFormField`.  
The functions in `useUpdateForm` takes as argument the *result* of the validations (a boolean), rather than the validation functions.  
Hence the location of the input field edition data, the functions that handle it and the validations are separated. I wanted to try on this approach, which is different to that found in libraries like `formik`, where data storage, validations and handling are integrated.

In turn, custom field components (`NoleggioTextField` and `NoleggioDataField`) act as adapters between the data in the Redux store and the props of the input components. For Date fields, this simplifies the use of the `react-day-picker` hook.

### Customer consolidation
The decisions made by the user in the graphical customer consolidation are stored as a list of changes. Each change involves a kind (e.g. `DELETE_CURRENT_PHONE`) and a value (e.g. which of the possibly many phones attached to the customer was selected for deletion). 

## Companion domain hook
Besides the functions inclded in `useUpdateForm`, that ease the handling of state related to input fields, it is handy to have functions that performs queries on the data stored in the Redux slice about rental creation, e.g. the rental total price or whether there are changes of some kind in the customer consolidation process.  
Since these functions need to access the Redux store, I decided to define a `useRentalCreation` hook that is a sort of "compoanion" for the pages and components that make up the rental creation UI.  
Just to try on, I made `useRentalCreation` kind of "inherit" `useUpdateForm`, by importing the functions of that hook and re-exporting them.


# Graphical appearance
Several MaterialUI components are present, including: `Grid`, `Stack`, `Box`, `Typography`, `Dialog`, `Card`, `Table`, `Tooltip` and `Avatar`. I also defined a (tiny) custom theme, and made some experiments with the color palette proposed by MUI 5.   

I wanted the information to span the complete window width for sizes up to `lg`, but only 4/5 of it for size `xl`, in order to avoid getting the information too much dispersed.
To this aim, I used the ability of defining the number of columns for the grid system.  
E.g for `RentalCustomerData` I organized the information in 8 columns, but I defined a grid with 40 columns, so that each "logical" column takes 5 "graphical" columns up to `lg`, and 4 for `xl` (so that 8 "graphical" columns, that is 20% of the space, keeps empty for larger resolutions). I defined helper components (e.g. `CellWithLabel`) that takes a number of "logical" columns in input, and transform it into the corresponding number of "graphical" columns.