import { configureStore } from "@reduxjs/toolkit"
import { rentalCreationSlice } from "./rentalCreationSlice"

export const storeActions = {
    rentalCreation: rentalCreationSlice.actions,
};

export const reduxStore = configureStore({
    reducer: {
        rentalCreation: rentalCreationSlice.reducer,
    },
    middleware: getDefaultMiddleware => getDefaultMiddleware({
        serializableCheck: {
            ignoredActionPaths: ['payload.dataRitiro', 'payload.dataRiconsegna'],
        },
    }),
});
