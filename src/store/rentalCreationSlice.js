import { createSlice } from "@reduxjs/toolkit";
import { isEqual } from "lodash";

export const CHANGE_TO_CUSTOMER_KINDS = {
    DELETE_CURRENT_PHONE: 'deleteCurrentPhone', DELETE_CURRENT_MAIL: 'deleteCurrentMail', 
    CHANGE_NAME: 'changeName', ADD_PHONE: 'addPhone', ADD_MAIL: 'addMail'
};

const sliceInitialState = {
    formState: {
        // value / inputValue / errorText / helperText / isValid
        dataRitiro: { isValid: false, },
        dataRiconsegna: { isValid: false, },
        luogoRitiro: { isValid: false, value: "" },
        luogoRiconsegna: { isValid: false, value: "" },
        customerName: { isValid: false, value: "", touched: false },
        customerPhoneNumber: { isValid: false, value: "", touched: false },
        customerMail: { isValid: false, value: "", touched: false },
    },
    availableCars: null, 
    customer: null, 
    car: null,
    changesToCustomer: [],
};

export const rentalCreationSlice = createSlice({
    name: 'rentalCreation',
    initialState: {...sliceInitialState},
    reducers: {
        updateFormField(state, action) {
            state.formState[action.payload.fieldName] = { ...state.formState[action.payload.fieldName], ...action.payload.updatedData }; 
        },

        setAvailableCars(state, action) { state.availableCars = action.payload; },

        clearAvailableCars(state) { state.availableCars = null; },

        setCar(state, action) { state.car = action.payload; },

        setCustomer(state, action) { state.customer = action.payload; },

        clearCustomer(state) { state.customer = null; }, 

        clearRental() { return {...sliceInitialState} },

        toggleChangeToCustomer(state, action) {
            const equalElement = state.changesToCustomer.find(change => isEqual(change, action.payload));
            if (equalElement) {
                state.changesToCustomer = state.changesToCustomer.filter(change => change !== equalElement);
            } else {
                state.changesToCustomer = [...state.changesToCustomer, action.payload];
            }
        },
    }
})

