import { TextField } from '@mui/material';
import { capitalize, pick, range } from 'lodash';
import { DateTime } from 'luxon';
import React, { useImperativeHandle, useRef } from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { dateStdFormat } from '../../../utils/dateFunctions';

const localeConfiguration = {
    formatDay: (date, locale) => DateTime.fromJSDate(date).setLocale(locale || 'en').toLocaleString(DateTime.DATETIME_MED_WITH_WEEKDAY),
    formatMonthTitle: (date, locale) => DateTime.fromJSDate(date).setLocale(locale || 'en').toLocaleString({ month: 'long', year: 'numeric' }),
    formatWeekdayShort: (dayOfWeek, locale) => {
        const today = DateTime.utc();
        const someSunday = today.minus({days: today.weekday});
        const length = locale === 'pt' ? 3 : 2;
        return capitalize(someSunday.plus({days: dayOfWeek}).setLocale(locale || 'en').toLocaleString({ weekday: 'short'}).slice(0,length));
    },
    formatWeekdayLong: (dayOfWeek, locale) => {
        const today = DateTime.utc() 
        const someSunday = today.minus({days: today.weekday})
        return capitalize(someSunday.plus({days: dayOfWeek}).setLocale(locale || 'en').toLocaleString({ weekday: 'long'}));
    },
    getFirstDayOfWeek: (locale) => locale ==='en' ? 0 : 1,
    getMonths: (locale) => range(12).map(monthOffset => capitalize(DateTime.utc()
        .startOf("year").plus({months: monthOffset}).setLocale(locale || "en").toLocaleString({ month: 'long'}))),
    formatDate: (date, format, locale) => {
        const realFormat = format || dateStdFormat;
        const realLocale = locale || "en";
        return DateTime.fromJSDate(date).setLocale(realLocale).toFormat(realFormat);
    },
    parseDate: (str, format, locale) => {
        const realFormat = format || dateStdFormat;
        const realLocale = locale || "en";
        const possibleDate = DateTime.fromFormat(str, realFormat, { locale: realLocale });
        return !!possibleDate.invalid ? undefined : possibleDate.toJSDate();
    },
}


/*
 * this component exposes a ref to itself, such ref expsoses the focus() and value() functions/methods.
 * Calls to focus() delegate to the inner HTML <input> element, while calls to value() yield that element' value.
 * 
 * The value() must be exposed as a function/method instead of an attribute, 
 * because actually accessing the ref to the <input> in the function passed to useImperativeHandle
 * cause strange problems, that could be related to whether the ref already exists at the time 
 * that function is evaluated.
 * By exposing a function instead, the ref is accessed only when the exposed function is invoked, this is after the component
 * has been rendered, and therefore the ref should be already defined.
 */
const textFieldForDate = React.forwardRef(
    (props, ref) => {
        const refToHtmlInput = useRef();

        // the useImperativeHandle hook allows to send a ref to the component which is being created,
        // and to specify which functions/methods the ref will expose.
        // cfr. https://reactjs.org/docs/hooks-reference.html#useimperativehandle 
        // ... where exactly this case (exposing the focus method that delegates to an inner HTML <input>) is given as example
        // The value() must be exposed as a function, cfr. explanation above.
        const focusInput = () => refToHtmlInput.current.focus();
        const valueOfInput = () => refToHtmlInput.current.value;
        useImperativeHandle(ref, () => { return { focus: focusInput, value: valueOfInput } })

        const propsToPass = pick(props, [
            "onKeyDown", "onKeyUp", "onFocus", "onBlur", "onChange", "onClick", "placeholder", "value", "error", "helperText"]);
        return <TextField variant="standard" inputRef={refToHtmlInput} {...propsToPass} />
    }
)

export const DateInputField = props => {
    return <DayPickerInput
        // use a custom TextField to edit the date
        component={textFieldForDate}

        // functions to transform between the JS Date handled by ReactDayPicker and the string that is shown in the edit field
        formatDate={localeConfiguration.formatDate} parseDate={localeConfiguration.parseDate}
        // to i18n the day picker component
        dayPickerProps={{
            locale: 'it',
            localeUtils: localeConfiguration,
            showOutsideDays: true,
            disabledDays: props.disabledDays,
        }}
        // these props are passed to textFieldForDate, which in turn passes them to TextField
        // I tried different alternatives to pass these props inside the component, like
        // component={<TextFieldForDate error={props.error} ...>} 
        // or textFieldForDate = (error, ...) => React.forwardRef( ... error ...)
        // or even move the definition of textFieldForDate inside DateInputField
        // but none worked.
        inputProps={{ error: props.error, helperText: props.helperText || "\xa0" }}
        // date format for the edit field, is passed to formatDate and parseDate functions
        format={dateStdFormat} 
        placeholder="DD/MM/AAAA" 
        // props passed to the day picker input
        value={props.value} onDayChange={props.onDayChange}
    />

    // for English
    // placeholder='MM/DD/YYYY'
    // format="MM/dd/yyyy"

    // alternative to directly pass a TextField as component, does not work since focus is lost each time a letter is typed
        // component={React.forwardRef((xprops, ref) => 
        //     // <TextField variant="standard" error={props.error} inputProps={{id: "fechona", "aria-describedby": "fechona-aria"}}
        //     //     helperText={props.helperText || "\xa0"} inputRef={ref} {...xprops} />
        //     <Input inputRef={ref} inputProps={{id: "fechona", "aria-describedby": "fechona-aria"}} {...xprops} />
        // )}
}


// console.log's to verify behavior of edited dates
// dataRitiro.normalized = DateTime.utc(dataRitiro.value.getFullYear(), dataRitiro.value.getMonth() + 1, dataRitiro.value.getDate());
// console.log(DateTime.fromJSDate(dataRitiro.value).toFormat("dd/MM/yyyy HH:mm ZZZ z"));
// console.log(dataRitiro.normalized.toFormat("dd/MM/yyyy HH:mm ZZZ z"));
// console.log(today().toFormat("dd/MM/yyyy HH:mm ZZZ z"));
