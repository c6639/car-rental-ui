import { TextField } from "@mui/material";
import { omit } from "lodash";
import { DateInputField } from "./DateInput";

export function NoleggioTextField(props) {
    const otherProps = omit(props, ["fieldState"]);
    const { value, errorText, helperText } = props.fieldState;
    return <TextField variant="standard" value={value}
        error={!!errorText}
        helperText={errorText || helperText || "\xa0"}
        {...otherProps}
    />
}

export function NoleggioDateField(props) {
    const otherProps = omit(props, ["fieldState"]);
    const { inputValue, errorText, helperText } = props.fieldState;
    return <DateInputField value={inputValue}
        error={!!errorText} helperText={errorText || helperText}
        {...otherProps}
    />
}