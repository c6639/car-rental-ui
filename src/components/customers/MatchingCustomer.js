import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import CardContent from "@mui/material/CardContent";
import Stack from "@mui/material/Stack";
import CardActionArea from "@mui/material/CardActionArea";
import { Box } from "@mui/system";

export function MatchingCustomerSelection(props) {
    return props.customers.length > 0 ? <MatchingCustomersList {...props} /> : <NoMatchingCustomers {...props} />;
}

function MatchingCustomersList(props) {
    return <Dialog open={true} onClose={props.onClose} fullWidth maxWidth="md">
        <DialogTitle>Trovate {props.customers.length} coincidenze</DialogTitle>
        <DialogContent>
            <Stack direction="column" alignItems="center">
                <Grid container direction="row" wrap="wrap" spacing={0}
                    sx={{ backgroundColor: "lightsteelblue", py: "1rem", px: "1rem", ml: "0.5rem", borderRadius: "10px", width: "98%", }}
                >
                    {props.customers.length === 1 && <Grid item xs={3} />}
                    {props.customers.map(customer => <Grid item xs={6} key={customer.mail}>
                        <Box sx={{ mx: ".7rem", my: ".7rem" }}>
                            <MatchingCustomerCard customer={customer} onSelect={customer => {
                                props.onSelect(customer);
                                props.onClose();
                            }}/>
                        </Box>
                    </Grid>)}
                </Grid>
            </Stack>
        </DialogContent>
        <DialogActions>
            <Button onClick={props.onClose}>Non è nessuno di questi</Button>
        </DialogActions>
    </Dialog>;
}

function pairOfLists(list) { 
    const reduction = list.reduce(({ix,lastList,pairs}, elem) => 
        ix === 0 ? { ix: 1, lastList: [elem], pairs } : { ix: 0, lastList: [], pairs: [...pairs, [...lastList, elem]]}, 
        { ix: 0, lastList: [], pairs: [] }
    );
    const { lastList, pairs } = reduction;
    return lastList.length === 0 ? pairs : [...pairs, lastList];
}

function MatchingCustomerCard(props) {
    const { name, phone, mail } = props.customer;

    const renderListInPairs = list => pairOfLists(list).map(pairs => {
        return <Box sx={{ typography: "dataItem", mb: "2px" }} key={pairs[0]}>
            {pairs[0]}{pairs.length === 2 && ` - ${pairs[1]}`}
        </Box>
    });

    return <Card>
        <CardActionArea onClick={() => props.onSelect(props.customer)}>
            <CardContent>
                <Stack direction="column">
                    <Box sx={{ typography: "label", mb: "6px", minWidth: "30rem" }}>{name}</Box>
                    {renderListInPairs(phone)}
                    {renderListInPairs(mail)}
                    {/* {mail.map(oneMail => <Box sx={{ typography: "dataItem", mb: "2px" }}>{oneMail}</Box>)} */}
                </Stack>
            </CardContent>
        </CardActionArea>
    </Card>;
}

function NoMatchingCustomers(props) {
    return <Dialog open={true} onClose={props.onClose}>
        <DialogTitle>Nessuna coincidenza</DialogTitle>
        <DialogContent>
            Non ci sono trovati clienti esistenti per i parametri di ricerca.
        </DialogContent>
        <DialogActions>
            <Button onClick={props.onClose}>Tornare al noleggio</Button>
        </DialogActions>
    </Dialog>;
}

