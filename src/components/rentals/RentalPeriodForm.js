import { Box, Button, Grid, Stack, Typography } from '@mui/material';
import React from 'react';
import { normalizedDate, today } from '../../utils/dateFunctions';
import { getAvailableCars } from '../../services/cars';
import { useDispatch, useSelector } from 'react-redux';
import { storeActions } from '../../store/reduxStore';
import { DateTime } from 'luxon';
import { NoleggioDateField, NoleggioTextField } from '../generic/form/NoleggioField';
import { useRentalCreation } from "../../domain/useRentalCreation";

function ritiroRiconsegnaValidation(dataRitiro, dataRiconsegna) {
    let message = null;
    if (dataRitiro && dataRiconsegna) {
        const normalizedRitiro = DateTime.fromMillis(dataRitiro);
        const normalizedRiconsegna = DateTime.fromMillis(dataRiconsegna);
        if (normalizedRiconsegna < normalizedRitiro) {
            message = "Non può essere prima del ritiro";
        } else if (normalizedRiconsegna > normalizedRitiro.plus({ months: 6 })) {
            message = "Non può essere oltre 6 mesi del ritiro";
        }
    }
    return message;
}

function riconsegnaValidation(dataRitiro, dataRiconsegna, inputRiconsegna) {
    return !dataRiconsegna && !inputRiconsegna
        ? "Dato obbligatorio"
        : !dataRiconsegna && inputRiconsegna
            ? "Serve una data valida"
            : ritiroRiconsegnaValidation(dataRitiro, dataRiconsegna);
}


export function RentalPeriodForm() {
    const storeData = useSelector(state => state.rentalCreation);
    const dispatchAction = useDispatch();
    const { dataRitiro, dataRiconsegna, luogoRitiro, luogoRiconsegna } = storeData.formState;
    const { updateTextField, updateDateField, updateFieldError, getDataRitiro, getDataRiconsegna, getDuration } = useRentalCreation();

    const framedLabelSx = {
        backgroundColor: "#F8F8FF", typography: "label",
        ml: "-8px", mr: "2rem", pl: "8px", borderRadius: "5px"
    };


    const giorniDiNoleggio = dataRitiro.value && dataRiconsegna.value && dataRitiro.isValid && dataRiconsegna.isValid
        ? getDuration() : null;

    return <>
        <Grid container direction="row" spacing={2} columns={30} sx={{mb:".5rem"}}>
            <Grid item sm={12} xl={10} >
                <Box sx={framedLabelSx}>
                    Ritiro
                </Box>
            </Grid>
            <Grid item sm={12} xl={10} >
                <Box sx={framedLabelSx}>
                    Riconsegna
                </Box>
            </Grid>
        </Grid>

        <Grid container direction="row" spacing={2} columns={30}>
            <Grid item sm={6} xl={5}>
                {/* <Grid item style={{marginRight: "3rem"}}> */}

                {/* Data di ritiro */}
                <Stack spacing={1}>
                    <Typography variant="body1" style={{ fontWeight: "bold" }}>Data</Typography>
                    <NoleggioDateField fieldState={dataRitiro} 
                        disabledDays={{ before: today().toJSDate() }}
                        onDayChange={(selectedDate, modifiers, dayPicker) => {
                            const isInThePast = selectedDate && normalizedDate(selectedDate) < today();
                            const errorText = !selectedDate && !dayPicker.getInput().value()
                                ? "Dato obbligatorio"
                                : !selectedDate && dayPicker.getInput().value()
                                    ? "Serve una data valida"
                                    : isInThePast ? "Dev'essere da oggi in poi" : null;
                            updateDateField('dataRitiro', selectedDate, dayPicker.getInput().value(), errorText);
                            updateFieldError('dataRiconsegna', riconsegnaValidation(
                                selectedDate && normalizedDate(selectedDate).toMillis(), 
                                dataRiconsegna.value, dataRiconsegna.inputValue
                            ));
                            dispatchAction(storeActions.rentalCreation.clearAvailableCars());
                        }}
                    />
                </Stack>
            </Grid>

            {/* Luogo di ritiro */}
            <Grid item sm={6} xl={5}>
                {/* <Grid item style={{marginRight: "5rem"}}> */}
                <Stack spacing={1} direction="column" style={{ paddingRight: "2rem" }}>
                    <Typography variant="body1" style={{ fontWeight: "bold" }}>Dove</Typography>
                    <NoleggioTextField fieldState={luogoRitiro} 
                        onChange={event => {
                            const value = event.target.value;
                            updateTextField('luogoRitiro', value, !value ? "Dato obbligatorio" : null);
                            dispatchAction(storeActions.rentalCreation.clearAvailableCars());
                        }}
                    />
                </Stack>
            </Grid>

            {/* Data di riconsegna */}
            <Grid item sm={6} xl={5}>
                {/* <Grid item style={{marginRight: "5rem"}}> */}
                <Stack spacing={1} direction="column">
                    <Typography variant="body1" style={{ fontWeight: "bold" }}>Data</Typography>
                    <NoleggioDateField fieldState={dataRiconsegna} 
                        disabledDays={dataRitiro.value
                            ? [{ before: new Date(dataRitiro.value) }, 
                                { after: getDataRitiro().plus({ months: 6 }).toJSDate() }]
                            : { before: today().toJSDate() }}
                        onDayChange={(selectedDate, modifiers, dayPicker) => {
                            updateDateField('dataRiconsegna', selectedDate, dayPicker.getInput().value(), 
                                riconsegnaValidation(dataRitiro.value, 
                                    selectedDate && normalizedDate(selectedDate).toMillis(), dayPicker.getInput().value())
                            );
                            dispatchAction(storeActions.rentalCreation.clearAvailableCars());
                        }}
                    />
                </Stack>
            </Grid>

            {/* Luogo di riconsegna */}
            <Grid item sm={6} xl={5}>
                {/* <Grid item style={{marginRight: "5rem"}}> */}
                <Stack spacing={1} direction="column" style={{ paddingRight: "2rem" }}>
                    <Typography variant="body1" style={{ fontWeight: "bold" }}>Dove</Typography>
                    <NoleggioTextField fieldState={luogoRiconsegna} 
                        onChange={event => {
                            const value = event.target.value;
                            updateTextField('luogoRiconsegna', value, !value ? "Dato obbligatorio" : null);
                            dispatchAction(storeActions.rentalCreation.clearAvailableCars());
                        }}
                    />
                </Stack>
            </Grid>

            {/* Button "cerca auto" */}
            <Grid item sm={6} xl={5}>
                {/* <Grid item> */}
                <Stack spacing={1} direction="column"
                    justifyContent="flex-start" alignItems="flex-start" style={{ marginLeft: "2rem" }}>
                    <Typography variant="body1" style={{ fontWeight: "bold" }}>&nbsp;</Typography>
                    <Button variant="contained" color="info" style={{
                        marginTop: "-1px", textTransform: "none", paddingLeft: "2rem", paddingRight: "2rem"
                    }}
                        onClick={() => {
                            const fetchAvailableCars = async () => {
                                const obtainedAvailableCars = await getAvailableCars(getDataRitiro(), getDataRiconsegna());
                                dispatchAction(storeActions.rentalCreation.setAvailableCars(obtainedAvailableCars));
                            }
                            fetchAvailableCars();
                    }} disabled={!dataRitiro.isValid || !dataRiconsegna.isValid || !luogoRitiro.isValid || !luogoRiconsegna.isValid || !!storeData.availableCars}
                    >
                        Cerca auto
                    </Button>
                </Stack>
            </Grid>
        </Grid>
 
        {/* Rental duration */}
        {giorniDiNoleggio &&
            <Grid container direction="row" columns={30} style={{ paddingRight: "2rem" }}>
                <Grid item sm={24} xl={20}>
                    <Typography variant="body2" style={{
                        color: "SteelBlue", backgroundColor: "AliceBlue",
                        paddingLeft: "1rem", paddingTop: "3px", paddingBottom: "3px",
                        marginRight: "1rem", borderRadius: "5px", textAlign: "center"
                    }}>{giorniDiNoleggio} {giorniDiNoleggio === 1 ? "giorno" : "giorni"} di noleggio</Typography>
                </Grid>
            </Grid>
        }
    </>
}
