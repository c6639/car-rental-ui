import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";

export function RentalWasAdded(props) {
    return <Dialog open={props.open} onClose={props.onClose}>
        <DialogTitle>Noleggio registrato</DialogTitle>
        <DialogContent>
            Premere il pulsante sottostante per chiudere l'operazione.
        </DialogContent>
        <DialogActions>
            <Button onClick={props.onClose}>Finire</Button>
        </DialogActions>
    </Dialog>;
}
