import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import CardContent from "@mui/material/CardContent";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { useDispatch, useSelector } from "react-redux";
import RemoveIcon from '@mui/icons-material/Remove';
import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft';
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight';
import SettingsBackupRestoreIcon from '@mui/icons-material/SettingsBackupRestore';
import { Avatar, Tooltip } from "@mui/material";
import { blueGrey, indigo, red, blue } from "@mui/material/colors";
import { useRentalCreation } from "../../domain/useRentalCreation";
import { storeActions } from "../../store/reduxStore";
import { CHANGE_TO_CUSTOMER_KINDS } from "../../store/rentalCreationSlice";
import { useEffect } from "react";

export function CustomerConsolidation(props) {
    const storeData = useSelector(state => state.rentalCreation);
    const dispatchAction = useDispatch();

    const { customerName, customerPhoneNumber, customerMail } = storeData.formState;
    const { changesForKind } = useRentalCreation();

    useEffect(() => {
        changesForKind(CHANGE_TO_CUSTOMER_KINDS.ADD_PHONE)
            .filter(change => change.value !== customerPhoneNumber.value)
            .forEach(change => dispatchAction(storeActions.rentalCreation.toggleChangeToCustomer(change)));
        changesForKind(CHANGE_TO_CUSTOMER_KINDS.ADD_MAIL)
            .filter(change => change.value !== customerMail.value)
            .forEach(change => dispatchAction(storeActions.rentalCreation.toggleChangeToCustomer(change)));
    }, [changesForKind, dispatchAction, customerPhoneNumber.value, customerMail.value],);

    return <Dialog open={props.open} onClose={props.onBack} fullWidth maxWidth="md">
        <DialogTitle>Aggiustare dati cliente</DialogTitle>
        <DialogContent>
            <Stack direction="column" alignItems="center">
                <Grid container direction="row" wrap="wrap" spacing={0}
                    sx={{ backgroundColor: indigo[50], py: "1rem", px: "1rem", ml: "0.5rem", borderRadius: "10px", width: "98%", }}
                >
                    <Grid item xs={6}>
                        <Stack direction="column" sx={{ mx: ".7rem", my: ".7rem" }}>
                            <Typography variant="h6" sx={{mb: "6px"}}>Dati in DB</Typography>
                            <Box>
                                <ExistingCustomer customer={storeData.customer}
                                    editedPhone={customerPhoneNumber.value} editedMail={customerMail.value} editedName={customerName.value}
                                />
                            </Box>
                        </Stack>
                    </Grid>
                    <Grid item xs={6}>
                        <Stack direction="column" sx={{ mx: ".7rem", my: ".7rem" }}>
                            <Typography variant="h6" sx={{ mb: "6px" }}>Dati inseriti ora</Typography>
                            <Box>
                                <NewData customer={storeData.customer}
                                    editedPhone={customerPhoneNumber.value} editedMail={customerMail.value} editedName={customerName.value}
                                />
                            </Box>
                        </Stack>
                    </Grid>
                </Grid>
            </Stack>
        </DialogContent>
        <DialogActions>
            <Button onClick={props.onBack}>Tornare a dati noleggio</Button>
            <Button onClick={props.onConfirm}>Registrare noleggio</Button>
        </DialogActions>
    </Dialog>;    
}


function ExistingCustomer(props) {
    const { name, phone, mail } = props.customer;
    const { editedName } = props;
    const { hasChange, changesForKind } = useRentalCreation();
    const dispatchAction = useDispatch();

    const deleteableCurrentItem = (value, change) => {
        const wantsToChange = hasChange(change);
        const variableStyleForValue = wantsToChange ? { color: blueGrey[300], textDecoration: "line-through" } : {};
        const variableStyleForButton = wantsToChange ? { bgcolor: indigo[300] } : { bgcolor: red[600] };
        return <Box sx={{ mb: "10px"}} key={value}>
            <Stack direction="row" justifyContent="space-between" alignItems="center">
                <Box sx={{ typography: "dataItem", ...variableStyleForValue }}>{value}</Box>
                <Tooltip title={wantsToChange ? "Cancellare l'eliminazione" : "Eliminare"}>
                    <Avatar sx={{ width: 20, height: 20, ...variableStyleForButton }} variant="rounded"
                        onClick={() => dispatchAction(storeActions.rentalCreation.toggleChangeToCustomer(change))} >
                        {wantsToChange ? <SettingsBackupRestoreIcon /> : <RemoveIcon />}
                    </Avatar>
                </Tooltip>
            </Stack>
        </Box>
    };

    const currentName = () => {
        const wantsToChange = hasChange({kind: CHANGE_TO_CUSTOMER_KINDS.CHANGE_NAME, value: editedName});
        const variableStyleForValue = wantsToChange ? { color: blueGrey[300], textDecoration: "line-through" } : {};
        return <Box sx={{ typography: "label", mb: wantsToChange ? "2px" : "10px", minWidth: "30rem", ...variableStyleForValue }}>{name}</Box>
    }

    const reverseableChange = (change, customTypography) => {
        const editedValue = change.value;
        return <Box sx={{ mb: "10px" }} key={`${change.kind}-${editedValue}`}>
            <Stack direction="row" justifyContent="space-between" alignItems="center">
                <Box sx={{ typography: customTypography || "dataItem", color: blue[700] }}>{editedValue}</Box>
                <Tooltip title={change.kind === CHANGE_TO_CUSTOMER_KINDS.CHANGE_NAME ? "Cancellare la modifica del nome" : "Cancellare l'aggiunta"}>
                    <Avatar sx={{ width: 20, height: 20, bgcolor: indigo[300] }} variant="rounded"
                        onClick={() => dispatchAction(storeActions.rentalCreation.toggleChangeToCustomer(change))} >
                        <KeyboardDoubleArrowRightIcon />
                    </Avatar>
                </Tooltip>
            </Stack>
        </Box>
    };

    return <Card>
        <CardContent>
            <Stack direction="column">
                {/* <Box sx={{ typography: "label", mb: "10px", minWidth: "30rem" }}>{name}</Box> */}
                {currentName()}
                {hasChange({ kind: CHANGE_TO_CUSTOMER_KINDS.CHANGE_NAME, value: editedName }) &&
                    reverseableChange({ kind: CHANGE_TO_CUSTOMER_KINDS.CHANGE_NAME, value: editedName }, "label")}
                {phone.map(onePhone => deleteableCurrentItem(
                    onePhone, {kind: CHANGE_TO_CUSTOMER_KINDS.DELETE_CURRENT_PHONE, value: onePhone}))}
                {changesForKind(CHANGE_TO_CUSTOMER_KINDS.ADD_PHONE).map(change => reverseableChange(change))}
                {mail.map(oneMail => deleteableCurrentItem(
                    oneMail, { kind: CHANGE_TO_CUSTOMER_KINDS.DELETE_CURRENT_MAIL, value: oneMail }))}
                {changesForKind(CHANGE_TO_CUSTOMER_KINDS.ADD_MAIL).map(change => reverseableChange(change))}
            </Stack>
        </CardContent>
    </Card>;
}


function NewData(props) {
    const { name, phone, mail } = props.customer;
    const { editedName, editedPhone, editedMail } = props;
    const { hasChange } = useRentalCreation();
    const dispatchAction = useDispatch();

    const nameChanged = editedName !== name;
    const phoneIsNew = !phone.includes(editedPhone);
    const mailIsNew = !mail.includes(editedMail);

    const appendeableItem = (valueChanged, change, typography, editedValue) => {
        const wantsToChange = hasChange(change);
        const variableStyleForValue = wantsToChange ? { color: blue[700] } : {};
        return <Box sx={{ mb: "10px" }}>
            <Stack direction="row" justifyContent="space-between" alignItems="center">
                <Box sx={{ typography, ...variableStyleForValue }}>{editedValue}</Box>  
                {valueChanged && !wantsToChange && 
                    <Tooltip title="Aggiungere alla DB">
                        <Avatar sx={{ width: 20, height: 20, bgcolor: indigo["A700"] }} variant="rounded"
                            onClick={() => dispatchAction(storeActions.rentalCreation.toggleChangeToCustomer(change))} >
                            <KeyboardDoubleArrowLeftIcon />
                        </Avatar>
                    </Tooltip>
                }
            </Stack>
        </Box>
    }

    return <Card>
        <CardContent>
            <Stack direction="column">
                {appendeableItem(nameChanged, { kind: CHANGE_TO_CUSTOMER_KINDS.CHANGE_NAME, value: editedName }, "label", editedName)}
                {appendeableItem(phoneIsNew, { kind: CHANGE_TO_CUSTOMER_KINDS.ADD_PHONE, value: editedPhone }, "dataItem", editedPhone)}
                {appendeableItem(mailIsNew, { kind: CHANGE_TO_CUSTOMER_KINDS.ADD_MAIL, value: editedMail }, "dataItem", editedMail)}
            </Stack>
        </CardContent>
    </Card>;
}

