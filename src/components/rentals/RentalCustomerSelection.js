import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useSelector } from "react-redux";
import { CellWithChildren, CellWithLabel, framedLabelSx } from '../../components/rentals/RentalCustomerDataHelperComponents';
import { NoleggioTextField } from "../generic/form/NoleggioField";
import { useRentalCreation } from "../../domain/useRentalCreation";
import { getMatchingCustomers } from "../../services/customers";

export function RentalCustomerSelection(props) {
    const { setMatchingCustomers } = props;

    const storeData = useSelector(state => state.rentalCreation);
    const { customerName, customerPhoneNumber, customerMail } = storeData.formState;
    const { updateTextField, clearTextField } = useRentalCreation();

    const customerDataIsEmpty = customerName.value === "" && customerPhoneNumber.value === "" && customerMail.value === "";

    return <>
        <Grid container direction="row" spacing={2} columns={40} style={{ marginBottom: ".5rem" }}>
            <CellWithLabel cols={6} label="Cliente" sx={framedLabelSx} />
        </Grid>
        <Grid container direction="row" spacing={2} columns={40} style={{ marginBottom: ".5rem" }}>
            <CellWithLabel cols={2} label="Nome e cognome" />
            <CellWithLabel cols={2} label="Telefono" />
            <CellWithLabel cols={2} label="Mail" />
        </Grid>
        <Grid container direction="row" spacing={2} columns={40}>
            <CellWithChildren cols={2}>
                <Box sx={{ mr: "2rem" }}>
                    <NoleggioTextField fieldState={customerName} fullWidth variant="standard"
                        onChange={event => {
                            const value = event.target.value;
                            updateTextField('customerName', value, !value ? "Dato obbligatorio" : null, true);
                        }}
                    />
                </Box>
            </CellWithChildren>
            <CellWithChildren cols={2}>
                <Box sx={{ mr: "2rem" }}>
                    <NoleggioTextField fieldState={customerPhoneNumber} fullWidth variant="standard"
                        onChange={event => {
                            updateTextField('customerPhoneNumber', event.target.value, null, true);
                        }}
                    />
                </Box>
            </CellWithChildren>
            <CellWithChildren cols={2}>
                <Box sx={{ mr: "2rem" }}>
                    <NoleggioTextField fieldState={customerMail} fullWidth variant="standard"
                        onChange={event => {
                            updateTextField('customerMail', event.target.value, null, true);
                        }}
                    />
                </Box>
            </CellWithChildren>
            <CellWithChildren cols={2}>
                <Stack direction="row">
                    <Button variant="contained" size="small" color="primary" sx={{ textTransform: "none", px: "1.5rem", mr: "2rem" }}
                        disabled={customerDataIsEmpty}
                        onClick={() => {
                            async function fetchMatchingCustomers() {
                                const foundMatchingCustomers = await getMatchingCustomers(customerName.value, customerPhoneNumber.value, customerMail.value);
                                setMatchingCustomers(foundMatchingCustomers);
                            }
                            fetchMatchingCustomers();
                        }}
                    >
                        Cerca
                    </Button>
                    <Button variant="contained" size="small" color="primary" sx={{ textTransform: "none", px: "1.5rem", }}
                        disabled={customerDataIsEmpty && !customerName.errorText && !customerPhoneNumber.errorText && !customerMail.errorText}
                        onClick={() => {
                            clearTextField("customerName");
                            clearTextField("customerPhoneNumber");
                            clearTextField("customerMail");
                        }}
                    >
                        Pulisci
                    </Button>
                </Stack>
            </CellWithChildren>
        </Grid>

        {!customerDataIsEmpty && <Grid container direction="row" spacing={2} columns={40} sx={{ mt: "2px" }}>
            <CellWithChildren cols={6} sx={{ pr: "1rem" }}>
                <Typography variant="body2" style={{
                    color: "SteelBlue", backgroundColor: "AliceBlue",
                    paddingLeft: "1rem", paddingTop: "3px", paddingBottom: "3px",
                    marginRight: "1rem", borderRadius: "5px", textAlign: "center"
                }}>{storeData.customer ? "Cliente esistente" : "Cliente nuovo"}</Typography>
            </CellWithChildren>
        </Grid>}

        <Grid container direction="row" spacing={0} columns={40} alignItems="baseline" sx={{ my: "1rem" }}>
            {/* <Grid item xs={35} xl={28} sx={{ color: "steelblue" }}>
                <div style={{ borderBottomWidth: "2px", borderBottomStyle: "solid", borderBottomColor: "darkblue", marginRight: "2rem" }}></div>
            </Grid> */}
        </Grid>    
    </>
}