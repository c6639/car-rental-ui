import { Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { storeActions } from "../../store/reduxStore";
import { useRentalCreation } from "../../domain/useRentalCreation";

function TitleText(props) {
    return <Typography variant="subtitle1" sx={{ fontWeight: "bold" }}>{props.children}</Typography>
}

export function AvailableCarList() {
    const rentalCreationData = useSelector(state => state.rentalCreation);
    const dispatchAction = useDispatch();
    const navigate = useNavigate();
    const { getTotalPrice } = useRentalCreation();
    
    return <TableContainer component="div">
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell colSpan={3} sx={{ borderWidth: 0 }}>
                        <Typography variant="h5" sx={{ fontWeight: "bold", color: "DarkBlue" }}>
                            {rentalCreationData.availableCars.length} vetture disponibili
                        </Typography>
                    </TableCell>
                </TableRow>
                <TableRow>
                    <TableCell><TitleText>Patente</TitleText></TableCell>
                    <TableCell><TitleText>Macchina</TitleText></TableCell>
                    <TableCell align='right'><TitleText>&euro;/Giorno</TitleText></TableCell>
                    <TableCell align='right'><TitleText>&euro; noleggio</TitleText></TableCell>
                    <TableCell><TitleText>&nbsp;</TitleText></TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {rentalCreationData.availableCars.map(car => <TableRow key={car.plate}>
                    <TableCell>{car.plate}</TableCell>
                    <TableCell>{car.description}</TableCell>
                    <TableCell align='right' >
                        <span style={{ paddingRight: "1rem" }}>{car.pricePerDay}</span>
                    </TableCell>
                    <TableCell align='right' style={{ paddingRight: "1rem" }}>
                        <span style={{ paddingRight: "1rem" }}>{getTotalPrice(car)}</span>
                    </TableCell>
                    <TableCell>
                        <Button variant="text" size="small" color="info" onClick={() => {
                            dispatchAction(storeActions.rentalCreation.setCar(car));
                            navigate("/rental/customerData");
                        }}>Noleggiare</Button>
                    </TableCell>
                </TableRow>)}
            </TableBody>
        </Table>
    </TableContainer>
}