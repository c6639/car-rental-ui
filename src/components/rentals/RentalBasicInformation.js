import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { CellWithChildren, CellWithData, CellWithLabel, framedLabelSx } from '../../components/rentals/RentalCustomerDataHelperComponents';
import { useRentalCreation } from "../../domain/useRentalCreation";

export function RentalBasicInformation() {
    const navigate = useNavigate();
    const storeData = useSelector(state => state.rentalCreation);
    const { dataRitiro, luogoRitiro, dataRiconsegna, luogoRiconsegna } = storeData.formState;
    const { getDuration, getTotalPrice } = useRentalCreation();

    return <>
        {/* Dates and places for the new rental */}
        <Grid container direction="row" spacing={2} columns={40} style={{ marginBottom: ".2rem" }}>
            <Grid item xs={10} xl={8} />
            <CellWithLabel cols={2} label="Ritiro" sx={framedLabelSx} />
            <CellWithLabel cols={2} label="Riconsegna" sx={framedLabelSx} />
        </Grid>
        <Grid container direction="row" spacing={2} columns={40} style={{ marginBottom: ".5rem" }}>
            <CellWithLabel cols={2} label="Macchina" />
            <CellWithLabel cols={1} label="Data" />
            <CellWithLabel cols={1} label="Dove" />
            <CellWithLabel cols={1} label="Data" />
            <CellWithLabel cols={1} label="Dove" />
        </Grid>
        <Grid container direction="row" spacing={2} columns={40} alignItems="baseline">
            <CellWithData cols={2} value={storeData.car.description} />
            <CellWithData cols={1} value={dataRitiro.inputValue} />
            <CellWithData cols={1} value={luogoRitiro.value} />
            <CellWithData cols={1} value={dataRiconsegna.inputValue} />
            <CellWithData cols={1} value={luogoRiconsegna.value} />
            <CellWithChildren cols={1}>
                <Button variant="contained" size="small" color="primary" sx={{ textTransform: "none", px: "2rem", }}
                    onClick={() => { navigate("/rental/initialParameters"); }}>
                    Cambia
                </Button>
            </CellWithChildren>
        </Grid>

        <Grid container direction="row" spacing={0} columns={40} alignItems="baseline" sx={{ my: "1rem" }}>
            {/* <Grid item xs={35} xl={28} sx={{ color: "steelblue" }}>
                <div style={{ borderBottomWidth: "2px", borderBottomStyle: "solid", borderBottomColor: "darkblue", marginRight: "2rem" }}></div>
            </Grid> */}
        </Grid>

        {/* Duration and price */}
        <Grid container direction="row" spacing={2} columns={40} style={{ marginBottom: ".5rem" }}>
            <CellWithChildren cols={2} />
            <CellWithLabel cols={2} label="Durata del noleggio" />
            <CellWithLabel cols={1} label="&euro;/giorno" />
            <CellWithLabel cols={1} label="&euro; noleggio" />
        </Grid>
        <Grid container direction="row" spacing={2} columns={40} alignItems="baseline">
            <CellWithChildren cols={2} />
            <CellWithData cols={2} value={getDuration() + " giorni"} />
            <CellWithData cols={1} value={storeData.car.pricePerDay} />
            <CellWithData cols={1} value={getTotalPrice()} />
        </Grid>
        <Grid container direction="row" spacing={0} columns={40} alignItems="baseline" sx={{ mt: "2rem", mb: "1.5rem" }}>
            <CellWithChildren cols={6}>
                <div style={{ borderBottomWidth: "2px", borderBottomStyle: "solid", borderBottomColor: "darkblue", marginRight: "2rem" }}></div>
            </CellWithChildren>
        </Grid>
    </>
}