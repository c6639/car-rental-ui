import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";

export function CellWithLabel(props) {
    return <Grid item xs={props.cols * 5} xl={props.cols * 4}>
        <Box sx={{ ...props.sx, typography: "label" }}>{props.label}</Box>
    </Grid>
}

export function CellWithData(props) {
    return <Grid item xs={props.cols * 5} xl={props.cols * 4}>
        <Box sx={{ ...props.sx, typography: "formFixedValue" }}>{props.value}</Box>
    </Grid>
}

export function CellWithChildren(props) {
    return <Grid item xs={props.cols * 5} xl={props.cols * 4}>
        {props.children}
    </Grid>
}

export const framedLabelSx = {
    backgroundColor: "#F8F8FF", typography: "label",
    ml: "-8px", mr: "2rem", pl: "8px", borderRadius: "5px"
};

