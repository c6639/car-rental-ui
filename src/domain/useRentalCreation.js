import { isEqual } from "lodash";
import { DateTime } from "luxon";
import { useSelector } from "react-redux";
import { useUpdateForm } from "../hooks/useUpdateForm";
import { storeActions } from "../store/reduxStore";
import { getRentalDuration, getRentalPrice } from "./rentalCalculations";

export function useRentalCreation() {
    const storeData = useSelector(state => state.rentalCreation);
    const { dataRitiro, dataRiconsegna } = storeData.formState;
    const { updateTextField, setTextField, updateDateField, updateFieldError, clearTextField } = useUpdateForm(storeActions.rentalCreation);

    function getDataRitiro() {
        return dataRitiro.value && DateTime.fromMillis(dataRitiro.value);
    }

    function getDataRiconsegna() {
        return dataRiconsegna.value && DateTime.fromMillis(dataRiconsegna.value);
    }

    function hasChange(change) {
        return !!storeData.changesToCustomer.find(existingChange => isEqual(existingChange, change));
    }

    function changesForKind(kind) {
        return storeData.changesToCustomer.filter(existingChange => existingChange === kind || existingChange.kind === kind );
    }

    function getDuration() {
        return getRentalDuration(getDataRitiro(), getDataRiconsegna());
    }

    function getTotalPrice(car = storeData.car) {
        return getRentalPrice(car, getDataRitiro(), getDataRiconsegna());
    }


    return { updateTextField, setTextField, updateDateField, updateFieldError, 
        clearTextField, getDataRitiro, getDataRiconsegna, hasChange, changesForKind, getDuration, getTotalPrice };
}