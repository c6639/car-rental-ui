export function getRentalDuration(dataRitiro, dataRiconsegna) {
    let rentalDuration = dataRitiro && dataRiconsegna && dataRitiro <= dataRiconsegna
        ? dataRiconsegna.diff(dataRitiro, "days").days
        : null;
    if (rentalDuration === 0) { rentalDuration = 1; }
    return rentalDuration;
}

export function getRentalPrice(car, dataRitiro, dataRiconsegna) {
    return car.pricePerDay * getRentalDuration(dataRitiro, dataRiconsegna);
}
