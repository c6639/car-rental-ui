import { pick } from "lodash";
import { today } from "../utils/dateFunctions";

const FAKE_CARS_DATA = [
    { plate: 'DZ308XA', description: 'Fiat Panda benzina 2da serie blu-celeste', pricePerDay: 45 },
    { plate: 'FG904YD', description: 'Audi A4 grigio', pricePerDay: 120, takenDays: 6 },
    { plate: 'CB783JB', description: 'Fiat Punto bianca 1.8', pricePerDay: 40, takenDays: 2 },
    { plate: 'DG814TT', description: 'Fiat Multipla 2.1', pricePerDay: 70 },
];

export async function getAvailableCars(fromDate, toDate) {
    const availableCarRecords = FAKE_CARS_DATA.filter(car => fromDate >= today().plus({days: car.takenDays || 0}));
    return Promise.resolve(availableCarRecords.map(car => pick(car, ["plate", "description", "pricePerDay"])));
}