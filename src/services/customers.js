const FAKE_CUSTOMERS = [
    { name: 'Rocco Granata', phone: ['9998887777'], mail: ['rocco@granata.xyz'] },
    { name: 'Maria Scirea', phone: ['8879980009', '7778880000', '8768769900'], mail: ['maria@scirea.xyz'] },
    { name: 'Gaetano Alberto Scirea', phone: ['29292921199'], mail: ['gaetano@scirea.xyz'] },
    { name: 'Giulia Scirea', phone: ['3459988772'], mail: ['giulia@scirea.xyz'] },
    { name: 'Peppino di capri', phone: ['8887776666'], mail: ['peppino@capri.cmp'] },
    { name: 'Adriano Celentano', phone: ['8887776666'], mail: ['adriano@celentano.cmp'] },
];

export async function getMatchingCustomers(name, phone, mail) {
    const isMatching = customer => (name && customer.name.toLowerCase().indexOf(name.toLowerCase()) > -1)
        || customer.phone.includes(phone) || customer.mail.includes(mail);
    return FAKE_CUSTOMERS.filter(isMatching);
}

