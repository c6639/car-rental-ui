import { Route, Routes } from 'react-router';
import { RentalCustomerData } from './pages/rentals/RentalCustomerData';
import { RentalInitialParameters } from "./pages/rentals/RentalInitialParameters";

function App() {
    return <Routes>
        <Route path="/rental/initialParameters" exact element={<RentalInitialParameters />} />
        <Route path="/rental/customerData" exact element={<RentalCustomerData />} />
        <Route path="" exact element={<RentalInitialParameters />} />
        <Route path="*" element={
            <div style={{ marginTop: "2rem", marginLeft: "3rem" }}>Page not found</div>
        } />
    </Routes>
}

export default App;
