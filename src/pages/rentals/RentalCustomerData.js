import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { MatchingCustomerSelection } from '../../components/customers/MatchingCustomer';
import { CustomerConsolidation } from '../../components/rentals/CustomerConsolidation';
import { CellWithChildren } from '../../components/rentals/RentalCustomerDataHelperComponents';
import { RentalWasAdded } from '../../components/rentals/RentalWasAdded';
import { useRentalCreation } from "../../domain/useRentalCreation";
import { storeActions } from '../../store/reduxStore';
import { RentalBasicInformation } from "../../components/rentals/RentalBasicInformation";
import { RentalCustomerSelection } from "../../components/rentals/RentalCustomerSelection";

export function RentalCustomerData() {
    const navigate = useNavigate();
    const storeData = useSelector(state => state.rentalCreation);
    const { customerName, customerPhoneNumber, customerMail } = storeData.formState;
    const dispatchAction = useDispatch();

    const { setTextField, updateFieldError } = useRentalCreation();
    const [matchingCustomers, setMatchingCustomers] = useState(null);
    const [rentalWasRegistered, setRentalWasRegistered] = useState(false);
    const [showCustomerConsolidation, setShowCustomerConsolidation] = useState(false);

    const customerDataIsEmpty = customerName.value === "" && customerPhoneNumber.value === "" && customerMail.value === "";
    const customerDataIsEnough = customerName.value !== "" && (customerPhoneNumber.value !== "" || customerMail.value !== "");
    const mustConsolidateCustomer = storeData.customer && (
        storeData.customer.name !== customerName.value 
        || (customerMail.value && !storeData.customer.mail.includes(customerMail.value)) 
        || (customerPhoneNumber.value && !storeData.customer.phone.includes(customerPhoneNumber.value))
    );

    useEffect(() => {
        if (customerDataIsEmpty) {
            dispatchAction(storeActions.rentalCreation.clearCustomer());
        }
    }, [customerDataIsEmpty, dispatchAction]);

    useEffect(() => {
        if (customerPhoneNumber.value === "" && customerMail.value === "" && (customerName.touched || customerPhoneNumber.touched || customerMail.touched)) {
            updateFieldError("customerPhoneNumber", "Si deve inserire telefono o mail");
        } else {
            updateFieldError("customerPhoneNumber", null);
        }
    }, [customerPhoneNumber.value, customerMail.value, customerName.touched, customerPhoneNumber.touched, customerMail.touched, updateFieldError]);

    const doRentalRegistration = () => { 
        console.log('-------------------------------')
        console.log('Registration of new rental confirmed')
        console.log(storeData)
        setRentalWasRegistered(true); 
    };

    return <Container maxWidth="xl">
         { matchingCustomers != null && <MatchingCustomerSelection customers={matchingCustomers} 
            onSelect={customer => {
                dispatchAction(storeActions.rentalCreation.setCustomer(customer));
                if (!customerName.touched || !customerName.value || customer.name.toLowerCase().indexOf(customerName.value.toLowerCase()) > -1) {
                    setTextField("customerName", customer.name);
                }
                if ((!customerPhoneNumber.touched || !customerPhoneNumber.value) && customer.phone.length > 0) {
                    setTextField("customerPhoneNumber", customer.phone[0]);
                }
                if ((!customerMail.touched || !customerMail.value) && customer.mail.length > 0) {
                    setTextField("customerMail", customer.mail[0]);
                }
            }}
            onClose={() => setMatchingCustomers(null)} /> }

        <CustomerConsolidation open={showCustomerConsolidation} existingCustomer={storeData.customer}
            onConfirm={() => { 
                setShowCustomerConsolidation(false); 
                doRentalRegistration();
            }} 
            onBack={() => { setShowCustomerConsolidation(false); }} 
        />

        <RentalWasAdded open={rentalWasRegistered} onClose={() => {
            dispatchAction(storeActions.rentalCreation.clearRental())
            navigate("/rental/initialParameters")
        }} />

        <Typography variant="h4" style={{ marginTop: "1rem", marginBottom: "1.5rem" }}>Nuovo noleggio</Typography>

        {/* Car, pickup & return coordinates, duration prices */}
        <RentalBasicInformation />

        {/* Customer information and search */}
        <RentalCustomerSelection setMatchingCustomers={setMatchingCustomers} />

        <Grid container direction="row" spacing={2} columns={40}>
            <CellWithChildren cols={6} />
            <CellWithChildren cols={2}>
                <Button variant="contained" color="success" sx={{ textTransform: "none", px: "2rem", }}
                    disabled={!customerDataIsEnough}
                    onClick={() => { 
                        if (mustConsolidateCustomer) {
                            setShowCustomerConsolidation(true);
                        } else {
                            doRentalRegistration();
                        }
                    }}>
                    Conferma nuovo noleggio
                </Button>
            </CellWithChildren>
        </Grid>
   </Container>
}
