import { Container, Grid, Typography } from '@mui/material';
import React from 'react';
import { useSelector } from 'react-redux';
import { AvailableCarList } from '../../components/rentals/AvailableCarList';
import { RentalPeriodForm } from '../../components/rentals/RentalPeriodForm';

export function RentalInitialParameters() {
    const { availableCars } = useSelector(state => state.rentalCreation);

    return (
    <Container maxWidth="xl">
        <Typography variant="h4" style={{marginTop: "1rem", marginBottom: "1.5rem"}}>Nuovo noleggio</Typography>

        <RentalPeriodForm />

        { availableCars != null && <>
            <div style={{ borderBottomWidth: "3px", borderBottomStyle: "solid", borderBottomColor: "DarkBlue", marginTop: ".75rem" }}>&nbsp;</div>
            <Grid container direction="row" style={{ marginTop: "1rem" }}>
                <Grid item sm={9} xl={6}>
                    {/* <Typography variant="body1" style={{
                            color: "SeaGreen", backgroundColor: "LightYellow", 
                            paddingLeft: "1rem", paddingTop: "3px", paddingBottom: "3px",
                            marginRight: "1rem", borderRadius: "5px", textAlign: "center"
                        }}>Trovate {availableCars.length} macchine disponibili</Typography> */}
                    <AvailableCarList />
                </Grid>
            </Grid>
        </>
        }
    </Container>
    )
}
