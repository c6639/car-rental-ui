import { createTheme } from "@mui/material/styles";

export const customMuiTheme = createTheme({
    typography: {
        label: {
            fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
            fontSize: "1rem", 
            fontWeight: "bold", 
            letterSpacing: "0.00938em", 
            lineHeight: 1.5,
        },
        dataItem: {
            fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
            fontSize: "0.9rem",
            fontWeight: "400",
            letterSpacing: "0.00938em",
            lineHeight: 1.2,
        },
        formFixedValue: {
            color: "steelblue",
        },
        h4: {
            color: "darkblue",
            fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
            fontWeight: "bold", 
            fontSize: "2.125rem", 
            letterSpacing: "0.00735em",
            lineHeight: 1.235,
        },
        h5: {
            color: "darkblue",
            fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
            fontWeight: "bold",
            fontSize: "1.5rem",
            lineHeight: 1.2,
        },
        h6: {
            color: "darkblue",
            fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
            fontWeight: "bold",
            fontSize: "1.2rem",
            lineHeight: 1.2,
        }
    }
});
