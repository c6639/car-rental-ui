import { DateTime } from "luxon";

export const dateStdFormat = "dd/MM/yyyy"

export function today() {
    return DateTime.utc().startOf("day");
}

export function normalizedDate(jsDateTime) {
    console.log(jsDateTime)
    return DateTime.utc(jsDateTime.getFullYear(), jsDateTime.getMonth() + 1, jsDateTime.getDate());
}

export function datePartsOK(year, month, day) {
    const date = new Date(Date.UTC(year, month, day));
    return date.getUTCFullYear() === year && year > 1000 && year < 4000
        && date.getUTCMonth() === month && date.getUTCDate() === day;
}

/*
 * These functions are needed because Redux does not like to store non-serializable data, in particular Luxon date representation.
 *
 * Cfr. https://redux.js.org/faq/organizing-state#can-i-put-functions-promises-or-other-non-serializable-items-in-my-store-state
 */
export function dateToRedux(date) {
    return date.toMillis();
}

export function uiDateToRedux(date) {
    return normalizedDate(date).toMillis();
}

export function dateFromRedux(reduxRepresentation) {
    return DateTime.fromMillis(reduxRepresentation);
}

export function uiDateFromRedux(reduxRepresentation) {
    return DateTime.fromMillis(reduxRepresentation).toFormat("dd/MM/yyyy");
}