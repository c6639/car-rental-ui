import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { LocalizationProvider } from '@mui/lab';
import LuxonDateAdapter from '@mui/lab/AdapterLuxon';
import './index.css';

import App from './App';
import { reduxStore } from './store/reduxStore';
import { ThemeProvider } from '@mui/system';
import { customMuiTheme } from './utils/customMuiTheme';

console.log(customMuiTheme);

ReactDOM.render(
    <LocalizationProvider dateAdapter={LuxonDateAdapter}>
        <Provider store={reduxStore}>
            <ThemeProvider theme={customMuiTheme}>
                <BrowserRouter>
                    <App />
                </BrowserRouter>
            </ThemeProvider>
        </Provider>
    </LocalizationProvider>,
    document.getElementById('root')
);

