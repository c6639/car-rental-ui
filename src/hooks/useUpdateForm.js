import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { normalizedDate } from "../utils/dateFunctions";

export function useUpdateForm(storeActions) {
    const dispatchAction = useDispatch();

    const updateTextField = (fieldName, value, errorText, markAsTouched) => {
        const updateObject = { value, errorText, isValid: !errorText, helperText: null };
        if (markAsTouched) {
            updateObject.touched = true;
        }
        dispatchAction(storeActions.updateFormField({
            fieldName,
            updatedData: updateObject,
        }));
    }

    const setTextField = (fieldName, value) => {
        dispatchAction(storeActions.updateFormField({
            fieldName,
            updatedData: { value, errorText: null, isValid: true, helperText: null, touched: false }
        }));
    }

    const clearTextField = (fieldName) => {
        dispatchAction(storeActions.updateFormField({
            fieldName,
            updatedData: { value: "", errorText: null, isValid: true, helperText: null, touched: false }
        }));
    }

    const updateDateField = (fieldName, dateValue, inputValue, errorText) => {
        dispatchAction(storeActions.updateFormField({
            fieldName,
            updatedData: {
                value: dateValue && normalizedDate(dateValue).toMillis(), inputValue,
                errorText, isValid: !errorText, helperText: null,
            }
        }));
    } 

    const updateFieldError = useCallback((fieldName, errorText) => {
        dispatchAction(storeActions.updateFormField({
            fieldName,
            updatedData: { errorText, isValid: !errorText, }
        }));
    }, [dispatchAction, storeActions]);

    return { updateTextField, setTextField, clearTextField, updateDateField, updateFieldError };
}
